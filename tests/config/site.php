<?php

return [
    'app' => [
        'name' => 'Platypus',
        'path' => dirname(__DIR__) . '/site'
    ],
    'twig' => [
        'cache' => false,
        'folders' => ['views', 'assets']
    ],
    'routes' => []
];
