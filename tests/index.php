<?php

$loader = require dirname(__DIR__) . '/vendor/autoload.php';


use Platypus\App;

App::setup(__DIR__ . '/config/site.php', $loader);
App::start();
