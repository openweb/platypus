<?php

namespace Platypus;

use Camel\CaseTransformer;
use Camel\Format;

class App
{
    public static function setup($config_path, $loader)
    {
        Config::load($config_path);

        $path = Config::get('app.path');

        $loader->addPsr4("Controllers\\", "{$path}/controllers");
        $loader->addPsr4("Models\\", "{$path}/models");
        $loader->addPsr4("Plugins\\", "{$path}/plugins");
        $loader->addPsr4("Hooks\\", "{$path}/hooks");

        $cache = Config::get('twig.cache');

        $folders = Config::get('twig.folders');
        $folders = array_map(function ($folder) use ($path) {
            $path = $folder . '/' . $path;
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            return $path;
        }, $folders);

        Twig::init($folders, $cache);
    }

    public static function start()
    {
        $routes = Config::get('routes', array());
        $languages = Config::get('languages', ['pt-br']);
        $applications = Config::get('applications', ['site']);

        Router::addMatchType(array(
            'i18n' => join('|', $languages),
            'app' => join('|', $applications)
        ));


        if (!empty($routes)) {
            foreach ($routes as $path => $callback) {
                Router::add($path, $callback);
            }
        }

        Router::add('/.[i18n:language]?/.[app:application]?/.[*:controller]?/.[*:action]?/.[**params]?', '\\Platypus\\App::route');
        Router::dispatch();
    }



    public static function route($context)
    {
        $language = $context['language'] ?? 'pt-br';
        Config::set('language', $language);

        $controller = $context['controller'] ?? 'home';
        $action = $context['action'] ?? 'index';
        $params = $context['params'] ?? '';
        $params = explode('/', trim($params, '/'));
        $params = array_filter($params, function ($param) {
            return '' != $param;
        });

        $transform = new CaseTransformer(new Format\SpinalCase, new Format\StudlyCaps);
        $controllerClass = '\\Controllers\\' . $transform->transform($controller);
        $actionMethod = $transform->transform($action);

        if (!class_exists($controllerClass)) {
            $controllerClass = '\\Controllers\\DefaultController';
        }

        if (!method_exists($controllerClass, $actionMethod)) {
            $actionMethod = 'Index';
        }

        $request = new Http\Request();
        $request->language = $language;
        $request->controller = $controller;
        $request->action = $action;
        $request->params = $params;

        $response = new Http\Response();



        call_user_func_array(array($controllerClass, $actionMethod), array($request, $response, $context));
    }
}
