<?php

$applications_dir = dirname(__DIR__).'/applications';

$applications = glob("{$applications_dir}/*", GLOB_ONLYDIR);
$applications = array_map(function($application) {
    return basename($application);
}, $applications);

return $applications;