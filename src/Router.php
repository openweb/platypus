<?php

namespace Platypus;

use Platypus\Http\Request;

class Router
{

    public static $routes = array();

    public static $matchTypes = array(
        'i' => '[0-9]++',
        'a' => '[0-9A-Za-z]++',
        'h' => '[0-9A-Fa-f]++',
        '*' => '.+?',
        '**' => '.++',
        '' => '[^\/\.]++'
    );

    public static function addRoutes($routes)
    {
        foreach ($routes as $route) {
            list($path, $target) = $route;
            static::addRoute($path, $target);
        }
    }

    public static function addRoute($path, $target)
    {
        static::$routes[] = array($path, $target);
    }

    public static function add($path, $target)
    {
        static::addRoute($path, $target);
    }

    public static function getRoutes()
    {
        return static::$routes;
    }

    public static function addMatchType($matchTypes)
    {
        static::$matchTypes = array_merge(static::$matchTypes, $matchTypes);
    }

    public static function compile($route)
    {
        if (preg_match_all('`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`', $route, $matches, PREG_SET_ORDER)) {
            $matchTypes = static::$matchTypes;
            foreach ($matches as $match) {
                list($block, $pre, $type, $param, $optional) = $match;
                if (isset($matchTypes[$type])) {
                    $type = $matchTypes[$type];
                }
                if ($pre === '.') {
                    $pre = '\.';
                }
                $optional = $optional !== '' ? '?' : null;

                //Older versions of PCRE require the 'P' in (?P<named>)
                $pattern = '(?:'
                    . ($pre !== '' ? $pre : null)
                    . '('
                    . ($param !== '' ? "?P<$param>" : null)
                    . $type
                    . ')'
                    . $optional
                    . ')'
                    . $optional;
                $route = str_replace($block, $pattern, $route);
            }
        }
        return "`^$route$`u";
    }

    public static function match($requestUrl = null)
    {
        $params = array();
        $match = false;

        if ($requestUrl === null) {
            $requestUrl = Request::PathInfo();
        }

        $routes = static::getRoutes();
        foreach ($routes as $handler) {
            list($route, $target) = $handler;
            if ($route == '*') {
                $match = true;
            } else if (isset($route[0]) && $route[0] === '@') {
                $pattern = '`' . substr($route, 1) . '`u';
                $match = preg_match($pattern, $requestUrl, $params) === 1;
            } else if (($position = strpos($route, '[')) === false) {
                $match = strcmp($requestUrl, $route) === 0;
            } else {
                if (strncmp($requestUrl, $route, $position) !== 0) {
                    //continue;
                }
                $regex = static::compile($route);
                $match = preg_match($regex, $requestUrl, $params) === 1;
            }

            if ($match) {
                if ($params) {
                    foreach ($params as $key => $value) {
                        if (is_numeric($key)) {
                            unset($params[$key]);
                        }
                    }
                }


                return (object) array(
                    'callback' => $target,
                    'context' => $params
                );
            }
        }

        return FALSE;
    }

    public static function dispatch($requestUri = null)
    {
        if ($requestUri == null) {
            $requestUri = Request::PathInfo();
        }
        $match = static::match($requestUri);
        return call_user_func_array($match->callback, array($match->context));
    }
}
